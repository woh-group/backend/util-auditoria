# Descripción

Proyecto de utilidades de auditoria

## Obtener todos los paquetes

go get -d -u gitlab.com/woh-group/backend/util-auditoria/...

## Crear Modulo

go mod init gitlab.com/woh-group/backend/util-auditoria
